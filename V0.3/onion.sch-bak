EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Omega2S+ U5
U 1 1 5EA6CA7A
P 3800 3350
F 0 "U5" H 3800 1161 50  0000 C CNN
F 1 "Omega2S+" H 3800 1070 50  0000 C CNN
F 2 "Module:Onion_Omega2S" H 3800 650 50  0001 C CNN
F 3 "https://github.com/OnionIoT/Omega2/raw/master/Documents/Omega2S%20Datasheet.pdf" H 4550 2300 50  0001 C CNN
	1    3800 3350
	1    0    0    -1  
$EndComp
Text Label 8150 3850 2    50   ~ 0
SD_CLK
Text Label 8450 3650 2    50   ~ 0
SD_CMD
Text Label 8450 3550 2    50   ~ 0
SD_D3
Text Label 8450 3450 2    50   ~ 0
SD_D2
$Comp
L Device:R_Small R15
U 1 1 5EA76FDA
P 6000 5050
F 0 "R15" H 6059 5096 50  0000 L CNN
F 1 "4.7k" H 6059 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 5050 50  0001 C CNN
F 3 "~" H 6000 5050 50  0001 C CNN
	1    6000 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5EA77FA9
P 6000 5250
F 0 "#PWR0129" H 6000 5000 50  0001 C CNN
F 1 "GND" H 6005 5077 50  0000 C CNN
F 2 "" H 6000 5250 50  0001 C CNN
F 3 "" H 6000 5250 50  0001 C CNN
	1    6000 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4850 6000 4950
Wire Wire Line
	6000 5150 6000 5250
Text HLabel 5200 2050 2    50   Input ~ 0
UART_TXD0
Text HLabel 5200 2150 2    50   Input ~ 0
UART_RXD0
Wire Wire Line
	4900 2050 5200 2050
Wire Wire Line
	4900 2150 5200 2150
Text HLabel 2400 5150 0    50   Input ~ 0
D+
Text HLabel 2400 5050 0    50   Input ~ 0
D-
Wire Wire Line
	2400 5050 2700 5050
Wire Wire Line
	2400 5150 2700 5150
$Comp
L Device:R_Small R20
U 1 1 5EA79FFB
P 9600 5600
F 0 "R20" V 9650 5750 50  0000 L CNN
F 1 "10k" V 9650 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 5600 50  0001 C CNN
F 3 "~" H 9600 5600 50  0001 C CNN
	1    9600 5600
	0    -1   -1   0   
$EndComp
Text GLabel 9000 5350 1    50   Input ~ 0
+3.3V
Text GLabel 7900 3450 1    50   Input ~ 0
+3.3V
$Comp
L power:GND #PWR0130
U 1 1 5EA7C762
P 7900 4500
F 0 "#PWR0130" H 7900 4250 50  0001 C CNN
F 1 "GND" H 7905 4327 50  0000 C CNN
F 2 "" H 7900 4500 50  0001 C CNN
F 3 "" H 7900 4500 50  0001 C CNN
	1    7900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4750 5400 4750
Wire Wire Line
	4900 4650 5400 4650
Wire Wire Line
	4900 4550 5400 4550
Wire Wire Line
	4900 4450 5400 4450
Wire Wire Line
	4900 4350 5400 4350
Wire Wire Line
	4900 4250 5400 4250
Wire Wire Line
	4900 4150 5400 4150
Text Label 5400 4250 0    50   ~ 0
SD_D1
Text Label 5400 4150 0    50   ~ 0
SD_D0
Text Label 5400 4650 0    50   ~ 0
SD_CLK
Text Label 5400 4550 0    50   ~ 0
SD_CMD
Text Label 5400 4450 0    50   ~ 0
SD_D3
Text Label 5400 4350 0    50   ~ 0
SD_D2
Wire Wire Line
	4900 4850 6000 4850
Text HLabel 5450 5150 2    50   Input ~ 0
I2C_SDA
Text HLabel 5450 5050 2    50   Input ~ 0
I2C_SCL
Text HLabel 2400 4550 0    50   Input ~ 0
I2S_CLK
Text HLabel 2400 4650 0    50   Input ~ 0
I2S_WS
Text HLabel 2400 4750 0    50   Input ~ 0
I2S_SDO
Wire Wire Line
	2400 4550 2700 4550
Wire Wire Line
	2400 4650 2700 4650
Wire Wire Line
	2400 4750 2700 4750
Text Label 3900 1000 1    50   ~ 0
VDD_FLASH
Text GLabel 3750 950  1    50   Input ~ 0
+3.3V
Wire Wire Line
	3700 1250 3700 1150
Wire Wire Line
	3700 1150 3750 1150
Wire Wire Line
	3800 1150 3800 1250
Wire Wire Line
	3750 950  3750 1150
Connection ~ 3750 1150
Wire Wire Line
	3750 1150 3800 1150
Wire Wire Line
	3900 1000 3900 1250
$Comp
L JPRIVA:MMSS8550 Q3
U 1 1 5E918A25
P 8900 1550
F 0 "Q3" V 9228 1550 50  0000 C CNN
F 1 "MMSS8550" V 9137 1550 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 9100 1475 50  0001 L CIN
F 3 "https://www.mccsemi.com/pdf/Products/MMSS8550(SOT-23).pdf" H 8900 1550 50  0001 L CNN
	1    8900 1550
	0    1    -1   0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5E91CC01
P 9450 1450
F 0 "R19" H 9509 1496 50  0000 L CNN
F 1 "50" H 9509 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9450 1450 50  0001 C CNN
F 3 "~" H 9450 1450 50  0001 C CNN
	1    9450 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R17
U 1 1 5E91E046
P 8500 1900
F 0 "R17" H 8559 1946 50  0000 L CNN
F 1 "10k" H 8559 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8500 1900 50  0001 C CNN
F 3 "~" H 8500 1900 50  0001 C CNN
	1    8500 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5E91F59B
P 9250 2100
F 0 "R18" H 9309 2146 50  0000 L CNN
F 1 "10k" H 9309 2055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9250 2100 50  0001 C CNN
F 3 "~" H 9250 2100 50  0001 C CNN
	1    9250 2100
	-1   0    0    1   
$EndComp
$Comp
L Transistor_FET:DMG2302U Q2
U 1 1 5E9280E2
P 8050 2250
F 0 "Q2" H 8254 2296 50  0000 L CNN
F 1 "DMG2302U" H 8254 2205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 8250 2175 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMG2302U.pdf" H 8050 2250 50  0001 L CNN
	1    8050 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5E92BEB1
P 9250 2600
F 0 "#PWR0131" H 9250 2350 50  0001 C CNN
F 1 "GND" H 9255 2427 50  0000 C CNN
F 2 "" H 9250 2600 50  0001 C CNN
F 3 "" H 9250 2600 50  0001 C CNN
	1    9250 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 1750 8900 1900
Wire Wire Line
	8900 1900 8600 1900
Wire Wire Line
	9250 2000 9250 1450
Wire Wire Line
	9250 1450 9100 1450
Wire Wire Line
	9350 1450 9250 1450
Connection ~ 9250 1450
Wire Wire Line
	9250 2600 9250 2550
Wire Wire Line
	8150 2550 9250 2550
Connection ~ 9250 2550
Wire Wire Line
	9250 2550 9250 2200
Wire Wire Line
	8150 2050 8150 1900
Wire Wire Line
	8150 1900 8400 1900
Text GLabel 8350 1300 1    50   Input ~ 0
+3.3V
Text Label 9850 1450 0    50   ~ 0
VDD_FLASH
Text Label 7550 2250 2    50   ~ 0
CPU_RST
Wire Wire Line
	9550 1450 9850 1450
Wire Wire Line
	8350 1300 8350 1450
Wire Wire Line
	8350 1450 8700 1450
Wire Wire Line
	7850 2250 7550 2250
Text Label 5200 1650 0    50   ~ 0
CPU_RST
Wire Wire Line
	4900 1650 5200 1650
$Comp
L Device:R_Small R13
U 1 1 5E940743
P 2000 2850
F 0 "R13" V 1950 2650 50  0000 L CNN
F 1 "4.7k" V 2050 2600 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2000 2850 50  0001 C CNN
F 3 "~" H 2000 2850 50  0001 C CNN
	1    2000 2850
	0    -1   -1   0   
$EndComp
Text GLabel 1300 2650 1    50   Input ~ 0
+3.3V
Wire Wire Line
	1300 2650 1300 2850
Wire Wire Line
	1300 2850 1450 2850
Wire Wire Line
	1750 2850 1900 2850
Wire Wire Line
	2100 2850 2700 2850
Text HLabel 5200 2350 2    50   Input ~ 0
UART_TXD1
Text HLabel 5200 2450 2    50   Input ~ 0
UART_RXD1
Wire Wire Line
	4900 2350 5200 2350
Wire Wire Line
	4900 2450 5200 2450
Text Label 1300 7000 0    50   ~ 0
SCL
Text Label 1600 7000 0    50   ~ 0
SDA
$Comp
L Device:R_Small R11
U 1 1 5EA3AADF
P 1300 6750
F 0 "R11" H 1359 6796 50  0000 L CNN
F 1 "2.2k" H 1359 6705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1300 6750 50  0001 C CNN
F 3 "~" H 1300 6750 50  0001 C CNN
	1    1300 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R12
U 1 1 5EA3AAE5
P 1600 6750
F 0 "R12" H 1659 6796 50  0000 L CNN
F 1 "2.2k" H 1659 6705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1600 6750 50  0001 C CNN
F 3 "~" H 1600 6750 50  0001 C CNN
	1    1600 6750
	1    0    0    -1  
$EndComp
Text GLabel 1300 6500 1    50   Input ~ 0
+3.3V
Text GLabel 1600 6500 1    50   Input ~ 0
+3.3V
Wire Wire Line
	1300 6850 1300 7000
Wire Wire Line
	1300 6500 1300 6650
Wire Wire Line
	1600 6500 1600 6650
Wire Wire Line
	1600 6850 1600 7000
Text Label 5300 5050 0    50   ~ 0
SCL
Text Label 5300 5150 0    50   ~ 0
SDA
Wire Wire Line
	4900 5050 5450 5050
Wire Wire Line
	4900 5150 5450 5150
Text HLabel 2300 2150 0    50   Input ~ 0
JTAG_TCK
Text HLabel 2300 2050 0    50   Input ~ 0
JTAG_TDI
Text HLabel 2300 1950 0    50   Input ~ 0
JTAG_TDO
Text HLabel 2300 1850 0    50   Input ~ 0
JTAG_TMS
Text HLabel 2300 1750 0    50   Input ~ 0
JTAG_TRST
Wire Wire Line
	2300 2150 2700 2150
Wire Wire Line
	2300 2050 2700 2050
Wire Wire Line
	2300 1950 2700 1950
Wire Wire Line
	2300 1850 2700 1850
Wire Wire Line
	2300 1750 2700 1750
Text HLabel 2350 3950 0    50   Input ~ 0
SPI_MISO
Text HLabel 2350 4050 0    50   Input ~ 0
SPI_MOSI
Text HLabel 2350 4150 0    50   Input ~ 0
SPI_CLK
Text HLabel 2350 4250 0    50   Input ~ 0
SPI_CS0
Text HLabel 2350 4350 0    50   Input ~ 0
SPI_CS1
Wire Wire Line
	2350 3950 2700 3950
Wire Wire Line
	2700 4050 2350 4050
Wire Wire Line
	2350 4150 2700 4150
Wire Wire Line
	2700 4250 2350 4250
Wire Wire Line
	2350 4350 2700 4350
$Comp
L Connector:Micro_SD_Card_Det_Hirose_DM3AT J2
U 1 1 5EA2D2B9
P 9650 3850
F 0 "J2" H 9600 4667 50  0000 C CNN
F 1 "Micro_SD_Card_Det_Hirose_DM3AT" H 9600 4576 50  0000 C CNN
F 2 "Connector_Card:microSD_HC_Hirose_DM3AT-SF-PEJM5" H 11700 4550 50  0001 C CNN
F 3 "https://www.hirose.com/product/en/download_file/key_name/DM3/category/Catalog/doc_file_id/49662/?file_category_id=4&item_id=195&is_series=1" H 9650 3950 50  0001 C CNN
	1    9650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3450 8450 3450
Wire Wire Line
	8750 3550 8450 3550
Wire Wire Line
	8750 3650 8450 3650
Wire Wire Line
	8750 4050 8450 4050
Wire Wire Line
	8750 4150 8450 4150
Wire Wire Line
	8750 3750 7900 3750
Wire Wire Line
	7900 3750 7900 3450
Wire Wire Line
	8750 3950 7900 3950
Wire Wire Line
	7900 3950 7900 4350
$Comp
L Device:R_Small R16
U 1 1 5EA7E44D
P 8450 3850
F 0 "R16" V 8500 3650 50  0000 L CNN
F 1 "33" V 8500 3950 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8450 3850 50  0001 C CNN
F 3 "~" H 8450 3850 50  0001 C CNN
	1    8450 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8150 3850 8350 3850
Wire Wire Line
	8550 3850 8750 3850
Text Label 5400 4750 0    50   ~ 0
SD_CD
Wire Wire Line
	8750 4350 7900 4350
Connection ~ 7900 4350
Wire Wire Line
	7900 4350 7900 4500
Wire Wire Line
	8750 4250 8450 4250
Text Label 9900 5800 0    50   ~ 0
SD_CMD
Text Label 9900 5700 0    50   ~ 0
SD_D3
Text Label 9900 5600 0    50   ~ 0
SD_D2
Text Label 8450 4250 2    50   ~ 0
SD_CD
Text Label 8450 4050 2    50   ~ 0
SD_D0
Text Label 8450 4150 2    50   ~ 0
SD_D1
Text Label 9900 6200 0    50   ~ 0
SD_CD
Text Label 9900 6000 0    50   ~ 0
SD_D0
Text Label 9900 6100 0    50   ~ 0
SD_D1
Text Label 9900 5900 0    50   ~ 0
SD_CLK
$Comp
L Device:R_Small R21
U 1 1 5EAB2360
P 9600 5700
F 0 "R21" V 9650 5850 50  0000 L CNN
F 1 "10k" V 9650 5450 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 5700 50  0001 C CNN
F 3 "~" H 9600 5700 50  0001 C CNN
	1    9600 5700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R22
U 1 1 5EAB418A
P 9600 5800
F 0 "R22" V 9650 5950 50  0000 L CNN
F 1 "10k" V 9650 5550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 5800 50  0001 C CNN
F 3 "~" H 9600 5800 50  0001 C CNN
	1    9600 5800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R23
U 1 1 5EAB5F75
P 9600 5900
F 0 "R23" V 9650 6050 50  0000 L CNN
F 1 "10k" V 9650 5650 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 5900 50  0001 C CNN
F 3 "~" H 9600 5900 50  0001 C CNN
	1    9600 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R24
U 1 1 5EAB7D5B
P 9600 6000
F 0 "R24" V 9650 6150 50  0000 L CNN
F 1 "10k" V 9650 5750 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 6000 50  0001 C CNN
F 3 "~" H 9600 6000 50  0001 C CNN
	1    9600 6000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R25
U 1 1 5EAB9B61
P 9600 6100
F 0 "R25" V 9650 6250 50  0000 L CNN
F 1 "10k" V 9650 5850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 6100 50  0001 C CNN
F 3 "~" H 9600 6100 50  0001 C CNN
	1    9600 6100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R26
U 1 1 5EABB856
P 9600 6200
F 0 "R26" V 9650 6350 50  0000 L CNN
F 1 "10k" V 9650 5950 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9600 6200 50  0001 C CNN
F 3 "~" H 9600 6200 50  0001 C CNN
	1    9600 6200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9000 5600 9500 5600
Wire Wire Line
	9000 5350 9000 5600
Wire Wire Line
	9000 5600 9000 5700
Wire Wire Line
	9000 5700 9500 5700
Connection ~ 9000 5600
Wire Wire Line
	9000 5700 9000 5800
Wire Wire Line
	9000 5800 9500 5800
Connection ~ 9000 5700
Wire Wire Line
	9000 5800 9000 5900
Wire Wire Line
	9000 5900 9500 5900
Connection ~ 9000 5800
Wire Wire Line
	9000 5900 9000 6000
Wire Wire Line
	9000 6000 9500 6000
Connection ~ 9000 5900
Wire Wire Line
	9000 6000 9000 6100
Wire Wire Line
	9000 6100 9500 6100
Connection ~ 9000 6000
Wire Wire Line
	9000 6100 9000 6200
Wire Wire Line
	9000 6200 9500 6200
Connection ~ 9000 6100
Wire Wire Line
	9700 6200 9900 6200
Wire Wire Line
	9700 6100 9900 6100
Wire Wire Line
	9700 6000 9900 6000
Wire Wire Line
	9700 5900 9900 5900
Wire Wire Line
	9700 5800 9900 5800
Wire Wire Line
	9700 5700 9900 5700
Wire Wire Line
	9700 5600 9900 5600
$Comp
L Device:R_Small R14
U 1 1 5EAFE56F
P 2350 2550
F 0 "R14" V 2300 2350 50  0000 L CNN
F 1 "4.7k" V 2400 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2350 2550 50  0001 C CNN
F 3 "~" H 2350 2550 50  0001 C CNN
	1    2350 2550
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 5EAFE575
P 1950 2550
F 0 "D6" H 2100 2500 50  0000 C CNN
F 1 "LED" H 1950 2650 50  0000 C CNN
F 2 "newLib:led" H 1950 2550 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS22-2000-226/LTST-S270KGKT.pdf" H 1950 2550 50  0001 C CNN
	1    1950 2550
	-1   0    0    1   
$EndComp
Text GLabel 1650 2350 1    50   Input ~ 0
+3.3V
Wire Wire Line
	1650 2350 1650 2550
Wire Wire Line
	1650 2550 1800 2550
Wire Wire Line
	2100 2550 2250 2550
Wire Wire Line
	2450 2550 2700 2550
Text HLabel 2300 1650 0    50   Input ~ 0
AUDIO_MUTE
Wire Wire Line
	2300 1650 2700 1650
Text HLabel 2300 2350 0    50   Input ~ 0
STM_BOOT0
Text HLabel 2300 2250 0    50   Input ~ 0
STM_NRST
Wire Wire Line
	2300 2350 2700 2350
Wire Wire Line
	2300 2250 2700 2250
$Comp
L Device:LED D5
U 1 1 5F3C8082
P 1600 2850
F 0 "D5" H 1750 2800 50  0000 C CNN
F 1 "LED" H 1600 2950 50  0000 C CNN
F 2 "newLib:led" H 1600 2850 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS22-2000-226/LTST-S270KGKT.pdf" H 1600 2850 50  0001 C CNN
	1    1600 2850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EDCF8C0
P 10650 4700
AR Path="/5E934C03/5EDCF8C0" Ref="#PWR?"  Part="1" 
AR Path="/5EA697B4/5EDCF8C0" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 10650 4450 50  0001 C CNN
F 1 "GND" H 10655 4527 50  0000 C CNN
F 2 "" H 10650 4700 50  0001 C CNN
F 3 "" H 10650 4700 50  0001 C CNN
	1    10650 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L?
U 1 1 5EDCF8C6
P 10650 4550
AR Path="/5E934C03/5EDCF8C6" Ref="L?"  Part="1" 
AR Path="/5EA697B4/5EDCF8C6" Ref="L8"  Part="1" 
F 0 "L8" H 10698 4596 50  0000 L CNN
F 1 "L_Small" H 10698 4505 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10650 4550 50  0001 C CNN
F 3 "https://www.murata.com/en-us/products/productdata/8796738650142/ENFA0003.pdf" H 10650 4550 50  0001 C CNN
	1    10650 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 4700 10650 4650
Wire Wire Line
	10650 4450 10650 4350
Wire Wire Line
	10450 4350 10650 4350
$Comp
L power:GND #PWR0142
U 1 1 5EC8F183
P 3800 5750
F 0 "#PWR0142" H 3800 5500 50  0001 C CNN
F 1 "GND" H 3805 5577 50  0000 C CNN
F 2 "" H 3800 5750 50  0001 C CNN
F 3 "" H 3800 5750 50  0001 C CNN
	1    3800 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5450 3800 5750
Wire Wire Line
	8150 2450 8150 2550
$EndSCHEMATC
